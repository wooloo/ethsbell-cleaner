FROM rustlang/rust:nightly AS builder
WORKDIR /app
COPY . .
RUN CARGO_HOME=./cargo_home cargo install --path .

FROM debian:stable-slim
RUN apt-get update && apt-get install -y libssl-dev ca-certificates && rm -rf /var/lib/apt/lists/*
COPY --from=builder /app/cargo_home/bin/bell-api /usr/local/bin/bell-api
CMD ["bell-api"]