// ETHSBell's API structs.
use serde::Deserialize;

#[derive(Deserialize)]
pub struct Period {
	pub start_time: String,
	pub end_time: String,
	pub period_notice: Option<String>,
	pub period_name: String,
}

#[derive(Deserialize)]
pub struct Schedule {
	pub name: String,
	pub period_array: Vec<Period>,
}

#[derive(Deserialize)]
#[allow(non_snake_case)]
pub struct EthsBellResponse {
	pub schedule: Option<Schedule>,
	pub theSlot: Option<String>,
	pub time: usize,
	pub theNextSlot: Option<String>,
	pub periodEndTime: Option<String>,
	pub endOfPreviousPeriod: usize,
	pub formattedDate: String,
	pub dayOfWeek: String,
	pub formattedTime: String,
	pub scheduleCode: Option<String>,
	pub isListingForDay: bool,
	#[serde(skip)]
	pub noSchedule: bool,
	pub schoolInSession: bool,
	pub school_id: String,
	pub theNextSlot_: isize,
	pub timeLeftInPeriod: isize,
	pub timeSinceLastPeriod: isize,
}
