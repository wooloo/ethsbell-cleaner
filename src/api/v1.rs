use crate::bell;
use bell::EthsBellResponse;
use chrono::{NaiveDateTime, NaiveTime};
#[cfg(feature = "ws")]
use reqwest::blocking::ClientBuilder;
#[cfg(feature = "ws")]
use rocket::Route;
#[cfg(feature = "ws")]
use rocket::{get, routes};
#[cfg(feature = "ws")]
use rocket_contrib::json::Json;
use serde::{Deserialize, Serialize};
use std::convert::TryInto;

#[derive(Serialize, Deserialize)]
pub struct Period {
	pub name: String,
	pub notice: Option<String>,
	pub start: NaiveTime,
	pub end: NaiveTime,
}
impl From<&bell::Period> for Period {
	fn from(pd: &bell::Period) -> Period {
		let name = pd.period_name.clone();
		let notice = pd.period_notice.clone();
		let start_arr: Vec<u32> = pd
			.start_time
			.split(":")
			.into_iter()
			.map(|x| x.parse().unwrap())
			.collect();
		let end_arr: Vec<u32> = pd
			.end_time
			.split(":")
			.into_iter()
			.map(|x| x.parse().unwrap())
			.collect();
		let start = NaiveTime::from_hms(start_arr[0], start_arr[1], 0);
		let end = NaiveTime::from_hms(end_arr[0], end_arr[1], 0);
		Period {
			name,
			notice,
			start,
			end,
		}
	}
}

#[derive(Serialize, Deserialize)]
pub struct Response {
	pub schedule: Vec<Period>,
	pub schedule_name: String,
	pub in_session: bool,
	pub remote_time: NaiveDateTime,
}
impl From<bell::EthsBellResponse> for Response {
	fn from(rs: bell::EthsBellResponse) -> Response {
		let (schedule, schedule_name) = match rs.schedule {
			Some(sch) => {
				let schedule = sch.period_array.iter().map(|x| Period::from(x)).collect();
				let schedule_name = sch.name;
				(schedule, schedule_name)
			}
			None => (vec![], "None".to_string()),
		};
		let in_session = rs.schoolInSession;
		let remote_time = NaiveDateTime::from_timestamp(rs.time.try_into().unwrap(), 0);
		Response {
			schedule,
			schedule_name,
			in_session,
			remote_time,
		}
	}
}

#[cfg(feature = "ws")]
pub fn routes() -> Vec<Route> {
	routes!(endpoint)
}

#[cfg(feature = "ws")]
#[get("/")]
fn endpoint() -> Result<Json<Response>, Json<String>> {
	let client = ClientBuilder::new()
		.danger_accept_invalid_certs(true) // ETHSBell's SSL cert is invalid and hasn't been fixed, this is the only way to make it work
		.build()
		.unwrap();
	let bell_res = match client.get("https://api.ethsbell.xyz/data").send() {
		Ok(r) => r,
		Err(e) => {
			return Err(Json(format!(
				"Error fetching data from ETHSBell: {}",
				e.to_string()
			)))
		}
	};
	let json_in = match bell_res.json::<EthsBellResponse>() {
		Ok(j) => j,
		Err(e) => {
			return Err(Json(format!(
				"Error parsing ETHSBell's response: {}",
				e.to_string()
			)))
		}
	};
	Ok(Json(json_in.into()))
}
