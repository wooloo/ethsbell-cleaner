#![feature(proc_macro_hygiene, decl_macro)]
#[cfg(feature = "ws")]
#[macro_use]
extern crate rocket;
pub mod api;
pub mod bell;

#[get("/")]
#[cfg(feature = "ws")]
fn root() -> &'static str {
	"This isn't a human-accessible service. \
	The API endpoint is at /api/v1, if you're interested. \
	There aren't any other API versions right now."
}

#[cfg(feature = "ws")]
fn main() {
	rocket::ignite()
		.mount("/", routes!(root))
		.mount("/api/v1", api::v1::routes())
		.launch();
}

#[cfg(not(feature = "ws"))]
fn main() {
	eprintln!("The bin target of this repository doesn't work without the ws feature.")
}
